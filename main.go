package main

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"github.com/spf13/pflag"
	"gitlab.com/Ma_124/ginlambda"
	"gitlab.com/Ma_124/ginlog"
	"net/http"
	"os"
	"strings"
)

var about = &ginlambda.About{
	Name:      "license-api",
	Version:   "2.0",
	EnvPrefix: "SRV_",
}

var (
	debugMode bool
	ignoreRobotsTxt bool
)

var log zerolog.Logger

func main() {
	opts := &ginlambda.Options{}
	fs := pflag.NewFlagSet("", pflag.ExitOnError)
	ginlambda.BindFlags(fs, opts)
	fs.BoolVar(&ignoreRobotsTxt, "ignore-robotstxt", false, "")
	fs.BoolVar(&debugMode, "debug", false, "")
	_ = fs.Parse(os.Args[1:])
	if debugMode {
		gin.SetMode(gin.DebugMode)
		log = zerolog.New(zerolog.NewConsoleWriter())
	} else {
		gin.SetMode(gin.ReleaseMode)
		log = zerolog.New(os.Stderr)
	}

	ginlambda.StartWithOptions(newEngine(), about, opts)
}

func newEngine() *gin.Engine {
	r := gin.New()

	r.GET("/foo", func(c *gin.Context) {
		c.String(200, "foo")
	})

	r.GET("/.netlify/functions/l", func(c *gin.Context) {
		c.String(200, ".netlify")
	})

	r.GET("/.netlify/functions/l/bar", func(c *gin.Context) {
		c.String(200, ".netlify/bar")
	})

	r.Use(ginlog.CustomLogger(log, zerolog.DebugLevel))
	r.Use(func(c *gin.Context) {
		s := c.Request.URL.RawPath
		if strings.HasSuffix(s, "/") {
			c.Redirect(http.StatusPermanentRedirect, strings.TrimRight(s, "/"))
			return
		}
		c.Next()
	})
	r.Use(gin.Recovery())
	return r
}
