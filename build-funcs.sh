#!/usr/bin/env bash

set -x -e -o pipefail

mkdir -p funcs

DEPS=$(go list -f '{{ range .Imports }}{{.}}{{"\n"}}{{ end }}' . | grep -E '^[^/]*\.')
echo "$DEPS"
eval "$(printf 'go get "'; echo "$DEPS" | sed ':a;N;$!ba;s/\n/" "/g;s/$/"/g')"

go build -o funcs/l
